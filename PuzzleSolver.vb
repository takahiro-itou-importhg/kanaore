﻿Option Explicit On

Module PuzzleSolver

    Public Enum SolveComplete
        COMPLETE_NOT_FINISH = 0     ' 完成していない単語がある
        COMPLETE_FINISH = 1         ' 解答が完了した
    End Enum

    Public Enum SolveWriteResult
        RET_ERROR_COLLISION = -1    ' 矛盾が発生した
        RET_NO_DETERMINISTIC = 0    ' 確定箇所が全くなかった
        RET_WRITE_SOME_CHARS = 1    ' 何文字か書き込んだ
    End Enum

    ''-------------------------------------------------------------------------
    ''    完成した単語を確認する。
    ''  他の単語の文字を決めた事によって完成した単語も確認する。
    ''
    ''  @retval     True    すべての単語が完成した。
    ''  @retval     False   それ以外。
    ''-------------------------------------------------------------------------
    Public Function checkCompleteWords(ByRef objPuzzle As PuzzleData) As SolveComplete
        Dim idxWord As Integer
        Dim curLen As Integer, wrdLen As Integer
        Dim xCol As Integer, yRow As Integer
        Dim blnRet As SolveComplete = SolveComplete.COMPLETE_FINISH

        With objPuzzle
            For idxWord = 1 To .m_numWords
                With .m_aryWrite(idxWord)
                    curLen = .nLen
                    xCol = .pData(curLen - 1).xCol
                    yRow = .pData(curLen - 1).yRow
                End With
                wrdLen = .m_aryWords(idxWord).nLen
                If (curLen = wrdLen) Then
                    ' 完成している。
                    Continue For
                End If

                ' フィールド上の文字を確認する。
                If (checkCharChain(idxWord, curLen, xCol, yRow, objPuzzle)) Then
                    .m_aryWrite(idxWord).nLen = wrdLen
                    Continue For
                End If

                ' 完成していない。
                blnRet = SolveComplete.COMPLETE_NOT_FINISH
            Next
        End With

        Return blnRet
    End Function

    ''-------------------------------------------------------------------------
    ''    各単語の現在の先端の位置で、
    ''  進むことができる方向を確認する。
    ''-------------------------------------------------------------------------
    Public Function checkValidDirections(ByRef objPuzzle As PuzzleData) As Integer
        Dim idxWord As Integer, numWord As Integer
        Dim nDetCnt As Integer
        Dim blnErr As Boolean

        blnErr = False
        nDetCnt = 0

        numWord = objPuzzle.m_numWords
        For idxWord = 1 To numWord
            If Not (checkValidDirectionForWord(idxWord, objPuzzle)) Then
                blnErr = True
            End If
        Next

        If (blnErr) Then
            Return (-1)
        End If

        With objPuzzle
            For idxWord = 1 To numWord
                With .m_aryWrite(idxWord)
                    With .pData(.nLen - 1)
                        If (.nDirCnt = 1) Then nDetCnt = nDetCnt + 1
                    End With
                End With
            Next
        End With

        Return nDetCnt
    End Function

    Public Function searchTrialCandidate( _
                ByRef objPuzzle As PuzzleData, _
                ByRef retIdxWord As Integer, ByRef retDirNext As PuzzleData.Direction) As Boolean
        Dim idxWord As Integer
        Dim wrdLen As Integer, curLen As Integer
        Dim xCol As Integer, yRow As Integer
        Dim dirSet As PuzzleData.DirSet
        Dim retChk As SolveWriteResult
        Dim objCopy As New PuzzleData

        With objPuzzle
            For idxWord = 1 To .m_numWords
                wrdLen = .m_aryWords(idxWord).nLen
                With .m_aryWrite(idxWord)
                    curLen = .nLen
                    If (curLen >= wrdLen) Then Continue For

                    With .pData(curLen - 1)
                        dirSet = .dirSet
                        xCol = .xCol
                        yRow = .yRow
                    End With
                End With
                If (dirSet And PuzzleData.DirSet.DIRSET_UP) Then
                    objCopy.copyFrom(objPuzzle)
                    writeCharacter(idxWord, curLen, PuzzleData.Direction.DIR_UP, objCopy)
                    retChk = solveDeterministic(objCopy)
                    If (retChk = SolveWriteResult.RET_ERROR_COLLISION) Then
                        retIdxWord = idxWord
                        retDirNext = PuzzleData.Direction.DIR_UP
                        Return True
                    End If
                End If
                If (dirSet And PuzzleData.DirSet.DIRSET_LEFT) Then
                    objCopy.copyFrom(objPuzzle)
                    writeCharacter(idxWord, curLen, PuzzleData.Direction.DIR_LEFT, objCopy)
                    retChk = solveDeterministic(objCopy)
                    If (retChk = SolveWriteResult.RET_ERROR_COLLISION) Then
                        retIdxWord = idxWord
                        retDirNext = PuzzleData.Direction.DIR_LEFT
                        Return True
                    End If
                End If
                If (dirSet And PuzzleData.DirSet.DIRSET_RIGHT) Then
                    objCopy.copyFrom(objPuzzle)
                    writeCharacter(idxWord, curLen, PuzzleData.Direction.DIR_RIGHT, objCopy)
                    retChk = solveDeterministic(objCopy)
                    If (retChk = SolveWriteResult.RET_ERROR_COLLISION) Then
                        retIdxWord = idxWord
                        retDirNext = PuzzleData.Direction.DIR_RIGHT
                        Return True
                    End If
                End If
                If (dirSet And PuzzleData.DirSet.DIRSET_DOWN) Then
                    objCopy.copyFrom(objPuzzle)
                    writeCharacter(idxWord, curLen, PuzzleData.Direction.DIR_DOWN, objCopy)
                    retChk = solveDeterministic(objCopy)
                    If (retChk = SolveWriteResult.RET_ERROR_COLLISION) Then
                        retIdxWord = idxWord
                        retDirNext = PuzzleData.Direction.DIR_DOWN
                        Return True
                    End If
                End If

            Next
        End With

        Return False
    End Function

    Public Function solveDeterministic(ByRef objPuzzle As PuzzleData) As SolveWriteResult
        Dim retRun As SolveWriteResult

        Do
            retRun = PuzzleSolver.writeDeterministicChars(objPuzzle)
            If (retRun = SolveWriteResult.RET_ERROR_COLLISION) Then
                Exit Do
            End If

            PuzzleSolver.checkCompleteWords(objPuzzle)
        Loop While (retRun = SolveWriteResult.RET_WRITE_SOME_CHARS)

        Return retRun
    End Function

    ''-------------------------------------------------------------------------
    ''    確定文字を書き込む。
    ''-------------------------------------------------------------------------
    Public Function writeCharacter( _
                ByVal idxWord As Integer, ByVal posNext As Integer, _
                ByVal dirNext As PuzzleData.Direction, ByRef objPuzzle As PuzzleData) As Boolean
        Dim chrDet As Char, chrTest As Char
        Dim xCol As Integer, yRow As Integer

        With objPuzzle
            chrDet = .m_aryWords(idxWord).sChrs(posNext)
            With .m_aryWrite(idxWord).pData(posNext - 1)
                xCol = .xCol
                yRow = .yRow
                .dirNext = dirNext
            End With

            Select Case dirNext
                Case PuzzleData.Direction.DIR_UP : yRow = yRow - 1
                Case PuzzleData.Direction.DIR_LEFT : xCol = xCol - 1
                Case PuzzleData.Direction.DIR_RIGHT : xCol = xCol + 1
                Case PuzzleData.Direction.DIR_DOWN : yRow = yRow + 1
            End Select

            chrTest = .m_aryField(xCol, yRow)
            If (chrTest <> " ") AndAlso (chrTest <> chrDet) Then
                ' 書き込み失敗
                Return False
            End If

            .m_aryField(xCol, yRow) = chrDet
            With .m_aryWrite(idxWord)
                With .pData(posNext)
                    .xCol = xCol
                    .yRow = yRow
                End With
                .nLen = posNext + 1
            End With
        End With

        Return True
    End Function

    ''-------------------------------------------------------------------------
    ''    確定文字を書き込む。
    ''-------------------------------------------------------------------------
    Public Function writeDeterministicChars(ByRef objPuzzle As PuzzleData) As SolveWriteResult
        Dim idxWord As Integer
        Dim retChk As Integer
        Dim curLen As Integer
        Dim dirNext As PuzzleData.Direction

        retChk = checkValidDirections(objPuzzle)
        If (retChk < 0) Then
            ' エラー（矛盾）が発生。
            Return SolveWriteResult.RET_ERROR_COLLISION
        ElseIf (retChk = 0) Then
            Return SolveWriteResult.RET_NO_DETERMINISTIC
        End If

        With objPuzzle
            For idxWord = 1 To .m_numWords
                With .m_aryWrite(idxWord)
                    curLen = .nLen
                    With .pData(curLen - 1)
                        If (.nDirCnt <> 1) Then Continue For

                        ' 答えが確定しているので書き込む。
                        dirNext = .dirNext
                    End With
                    If Not (writeCharacter(idxWord, curLen, dirNext, objPuzzle)) Then
                        ' エラー（矛盾）が発生。
                        Return SolveWriteResult.RET_ERROR_COLLISION
                    End If
                    '.nLen = .nLen + 1
                End With
            Next
        End With

        Return SolveWriteResult.RET_WRITE_SOME_CHARS
    End Function

    ''-------------------------------------------------------------------------
    ''    指定した座標にある文字チェックして、
    ''  指定した文字と一致するか、空白の場合には真を返す。
    ''-------------------------------------------------------------------------
    Private Function checkCharacter( _
                ByVal xCol As Integer, ByVal yRow As Integer, ByVal sChr As Char, _
                ByRef objPuzzle As PuzzleData) As Boolean
        Dim sTest As Char = objPuzzle.m_aryField(xCol, yRow)
        Return ((sTest = " ") OrElse (sTest = sChr))
    End Function

    ''-------------------------------------------------------------------------
    ''-------------------------------------------------------------------------
    Private Function checkCharChain( _
            ByVal idxWord As Integer, ByVal curLen As Integer, _
            ByVal xCol As Integer, ByVal yRow As Integer, _
            ByRef objPuzzle As PuzzleData) As Boolean
        Dim chrTest As Char
        Dim wrdLen As Integer

        With objPuzzle
            With .m_aryWords(idxWord)
                chrTest = .sChrs(curLen - 1)
                wrdLen = .nLen
            End With
            If Not (.m_aryField(xCol, yRow) = chrTest) Then
                Return False
            End If

            With .m_aryWrite(idxWord)
                With .pData(curLen - 1)
                    .xCol = xCol
                    .yRow = yRow

                    If (curLen = wrdLen) Then
                        Return True
                    End If

                    If (checkCharChain(idxWord, curLen + 1, xCol, yRow - 1, objPuzzle)) Then
                        .dirNext = PuzzleData.Direction.DIR_UP
                        Return True
                    End If
                    If (checkCharChain(idxWord, curLen + 1, xCol - 1, yRow, objPuzzle)) Then
                        .dirNext = PuzzleData.Direction.DIR_LEFT
                        Return True
                    End If
                    If (checkCharChain(idxWord, curLen + 1, xCol + 1, yRow, objPuzzle)) Then
                        .dirNext = PuzzleData.Direction.DIR_RIGHT
                        Return True
                    End If
                    If (checkCharChain(idxWord, curLen + 1, xCol, yRow + 1, objPuzzle)) Then
                        .dirNext = PuzzleData.Direction.DIR_DOWN
                        Return True
                    End If
                End With
            End With
        End With

        Return False
    End Function

    ''-------------------------------------------------------------------------
    ''    指定した座標の周囲に、指定した単語の残りの文字を
    ''  埋められるだけのスペースがあるか確認する。
    ''-------------------------------------------------------------------------
    Private Function checkEnoughSpace( _
                ByVal xCol As Integer, ByVal yRow As Integer, _
                ByVal idxWord As Integer, ByVal curLen As Integer, ByVal fixLen As Integer, _
                ByRef objPuzzle As PuzzleData) As Boolean
        Dim chrTemp As Char
        Dim chrPut As Char
        Dim blnRet As Boolean

        blnRet = False
        With objPuzzle
            With .m_aryWords(idxWord)
                If (curLen > .nLen) Then
                    Return True
                End If
                chrPut = .sChrs(curLen - 1)
            End With
            chrTemp = .m_aryField(xCol, yRow)

            If ((chrTemp <> " ") AndAlso (chrTemp <> chrPut)) Then
                ' この場所には既に違う文字が入っているので駄目。
                Return False
            End If

            ' 同じ単語内で使っている場所は使えないのでチェックする。
            For curPos = 0 To fixLen - 1
                With .m_aryWrite(idxWord).pData(curPos)
                    If ((.xCol = xCol) AndAlso (.yRow = yRow)) Then
                        Return False
                    End If
                End With
            Next

            ' 同じ単語内で使っている場所は使えないので番兵を書き込む。
            .m_aryField(xCol, yRow) = "□"

            ' 隣接する四箇所を確認する。
            ' 壱箇所でも OK なら、残りの箇所のチェックはスキップできる。
            If (checkEnoughSpace(xCol, yRow - 1, idxWord, curLen + 1, fixLen, objPuzzle)) Then
                blnRet = True
                GoTo Label_SkipChecks
            End If
            If (checkEnoughSpace(xCol - 1, yRow, idxWord, curLen + 1, fixLen, objPuzzle)) Then
                blnRet = True
                GoTo Label_SkipChecks
            End If
            If (checkEnoughSpace(xCol + 1, yRow, idxWord, curLen + 1, fixLen, objPuzzle)) Then
                blnRet = True
                GoTo Label_SkipChecks
            End If
            If (checkEnoughSpace(xCol, yRow + 1, idxWord, curLen + 1, fixLen, objPuzzle)) Then
                blnRet = True
                GoTo Label_SkipChecks
            End If

Label_SkipChecks:
            ' 一時的に書き込んだ番兵を元に戻す。
            .m_aryField(xCol, yRow) = chrTemp
        End With

        Return blnRet
    End Function

    ''-------------------------------------------------------------------------
    ''    指定した単語の現在の先端の位置で、
    ''  進むことができる方向を確認する。
    ''-------------------------------------------------------------------------
    Private Function checkValidDirectionForWord( _
                ByVal idxWord As Integer, _
                ByRef objPuzzle As PuzzleData) As Boolean
        Dim nowLen As Integer, wrdLen As Integer
        Dim xCol As Integer, yRow As Integer
        Dim dirSet As PuzzleData.DirSet
        Dim dirNext As PuzzleData.Direction
        Dim cntDirs As Integer
        Dim chrChk As Char

        With objPuzzle
            wrdLen = .m_aryWords(idxWord).nLen

            With .m_aryWrite(idxWord)
                nowLen = .nLen
                With .pData(nowLen - 1)
                    xCol = .xCol
                    yRow = .yRow
                    dirSet = .dirSet
                End With
            End With

            If (nowLen = wrdLen) Then
                Return True
            End If


            chrChk = .m_aryWords(idxWord).sChrs(nowLen)
            cntDirs = 0

            If (dirSet And PuzzleData.DirSet.DIRSET_UP) Then
                If (checkValidPos(xCol, yRow - 1, idxWord, nowLen, chrChk, objPuzzle)) Then
                    cntDirs = cntDirs + 1
                    dirNext = PuzzleData.Direction.DIR_UP
                Else
                    dirSet = dirSet - PuzzleData.DirSet.DIRSET_UP
                End If
            End If

            If (dirSet And PuzzleData.DirSet.DIRSET_LEFT) Then
                If (checkValidPos(xCol - 1, yRow, idxWord, nowLen, chrChk, objPuzzle)) Then
                    cntDirs = cntDirs + 1
                    dirNext = PuzzleData.Direction.DIR_LEFT
                Else
                    dirSet = dirSet - PuzzleData.DirSet.DIRSET_LEFT
                End If
            End If

            If (dirSet And PuzzleData.DirSet.DIRSET_RIGHT) Then
                If (checkValidPos(xCol + 1, yRow, idxWord, nowLen, chrChk, objPuzzle)) Then
                    cntDirs = cntDirs + 1
                    dirNext = PuzzleData.Direction.DIR_RIGHT
                Else
                    dirSet = dirSet - PuzzleData.DirSet.DIRSET_RIGHT
                End If
            End If

            If (dirSet And PuzzleData.DirSet.DIRSET_DOWN) Then
                If (checkValidPos(xCol, yRow + 1, idxWord, nowLen, chrChk, objPuzzle)) Then
                    cntDirs = cntDirs + 1
                    dirNext = PuzzleData.Direction.DIR_DOWN
                Else
                    dirSet = dirSet - PuzzleData.DirSet.DIRSET_DOWN
                End If
            End If

            With .m_aryWrite(idxWord).pData(nowLen - 1)
                .dirSet = dirSet
                If (cntDirs = 1) Then
                    .dirNext = dirNext
                End If
                .nDirCnt = cntDirs
            End With
        End With

        If (cntDirs = 0) Then
            Return False
        End If
        Return True
    End Function

    ''-------------------------------------------------------------------------
    ''    指定した座標が有効かどうか判定する。
    ''  有効である条件は、同一単語内で既に使われていない事、
    ''  その場所の文字が、指定した文字と一致するか空白である。
    ''-------------------------------------------------------------------------
    Private Function checkValidPos( _
                ByVal xCol As Integer, ByVal yRow As Integer, _
                ByVal idxWord As Integer, ByVal curLen As Integer, _
                ByVal chrChk As Char, _
                ByRef objPuzzle As PuzzleData) As Boolean
        Dim curPos As Integer

        ' 同一単語内で既に使われていないか確認する。
        With objPuzzle
            For curPos = 0 To curLen - 1
                With .m_aryWrite(idxWord).pData(curPos)
                    If ((.xCol = xCol) AndAlso (.yRow = yRow)) Then
                        Return False
                    End If
                End With
            Next
        End With

        ' フィールド上の文字を確認する。
        If Not (checkCharacter(xCol, yRow, chrChk, objPuzzle)) Then
            Return False
        End If

        ' 残りの文字が埋められるだけのスペースがあるか確認する。
        If Not (checkEnoughSpace(xCol, yRow, idxWord, curLen + 1, curLen, objPuzzle)) Then
            Return False
        End If

        Return True
    End Function

End Module
