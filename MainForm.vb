﻿Option Explicit On

Public Class frmMainForm

    Public m_objPuzzle As PuzzleData

    Private m_nSaveSP As Integer
    Private m_objSaveStack() As PuzzleData

    Public m_nCurSelectIdx As Integer

    Private m_fxLeftMargin As Integer = 8
    Private m_fyTopMargin As Integer = 8
    Private m_cxCellWidth As Integer = 32
    Private m_cyCellHeight As Integer = 32

    ''-------------------------------------------------------------------------
    ''    フィールドの升目を描画する。
    ''-------------------------------------------------------------------------
    Private Sub drawFieldLines(ByVal xCols As Integer, ByVal yRows As Integer, ByRef g As Graphics)
        Dim dpLeft As Integer, dpTop As Integer, dpEnd As Integer

        dpTop = m_fyTopMargin
        dpEnd = (yRows * m_cyCellHeight) + dpTop
        For I = 0 To xCols
            dpLeft = (I * m_cxCellWidth) + m_fxLeftMargin
            g.DrawLine(Pens.Black, dpLeft, dpTop, dpLeft, dpEnd)
        Next

        dpLeft = m_fxLeftMargin
        dpEnd = (xCols * m_cxCellWidth) + dpLeft
        For I = 0 To yRows
            dpTop = (I * m_cyCellHeight) + m_fyTopMargin
            g.DrawLine(Pens.Black, dpLeft, dpTop, dpEnd, dpTop)
        Next

    End Sub

    ''-------------------------------------------------------------------------
    ''    現在選択している単語を強調表示する。
    ''-------------------------------------------------------------------------
    Private Sub highlightSelectedWord( _
            ByVal nSelIdx As Integer, _
            ByRef objPuzzle As PuzzleData, ByRef g As Graphics)
        Dim xCol As Integer, yRow As Integer
        Dim idxChar As Integer, curLen As Integer, wrdLen As Integer
        Dim dpLeft As Integer, dpTop As Integer
        Dim prvLeft As Integer, prvTop As Integer
        Dim dirSet As PuzzleData.DirSet

        With objPuzzle
            wrdLen = .m_aryWords(nSelIdx + 1).nLen
            With .m_aryWrite(nSelIdx + 1)
                curLen = .nLen
                For idxChar = 0 To curLen - 1
                    With .pData(idxChar)
                        xCol = .xCol - 1
                        yRow = .yRow - 1
                    End With
                    dpLeft = (xCol * m_cxCellWidth) + m_fxLeftMargin + (m_cxCellWidth \ 2)
                    dpTop = (yRow * m_cyCellHeight) + m_fyTopMargin + (m_cyCellHeight \ 2)
                    If (idxChar > 0) Then
                        g.DrawLine(Pens.Magenta, prvLeft, prvTop, dpLeft, dpTop)
                    End If
                    prvLeft = dpLeft
                    prvTop = dpTop
                Next idxChar

                ' 文字の最後の部分では、進むことができる方向を表示する。
                If (curLen = wrdLen) Then Exit Sub
                With .pData(curLen - 1)
                    dirSet = .dirSet
                    If (dirSet And PuzzleData.DirSet.DIRSET_UP) Then
                        g.DrawLine(Pens.Green, prvLeft, prvTop, prvLeft, prvTop - m_cyCellHeight)
                    End If
                    If (dirSet And PuzzleData.DirSet.DIRSET_LEFT) Then
                        g.DrawLine(Pens.Green, prvLeft, prvTop, prvLeft - m_cxCellWidth, prvTop)
                    End If
                    If (dirSet And PuzzleData.DirSet.DIRSET_RIGHT) Then
                        g.DrawLine(Pens.Green, prvLeft, prvTop, prvLeft + m_cxCellWidth, prvTop)
                    End If
                    If (dirSet And PuzzleData.DirSet.DIRSET_DOWN) Then
                        g.DrawLine(Pens.Green, prvLeft, prvTop, prvLeft, prvTop + m_cyCellHeight)
                    End If
                End With
            End With
        End With

    End Sub

    ''-------------------------------------------------------------------------
    ''    フィールドおよびウィンドウのサイズを変更する。
    ''-------------------------------------------------------------------------
    Private Sub resizeField(ByRef objPuzzle As PuzzleData)
        Dim xCols As Integer, yRows As Integer
        Dim pfWidth As Integer, pfHeight As Integer

        With objPuzzle
            xCols = .m_numCols
            yRows = .m_numRows
        End With

        pfWidth = xCols * m_cxCellWidth + (m_fxLeftMargin * 2)
        pfHeight = yRows * m_cyCellHeight + (m_fyTopMargin * 2)

        With picField
            .Width = pfWidth
            .Height = pfHeight
        End With

        lstWords.Height = pfHeight
        With Me
            .Width = pfWidth + picField.Left + lstWords.Left * 2
            .Height = pfHeight + picField.Top + 16 + (Me.Height - Me.ClientSize.Height)
        End With
    End Sub

    ''-------------------------------------------------------------------------
    ''    フィールドの表示を更新する。
    ''-------------------------------------------------------------------------
    Private Sub updateField(ByRef objPuzzle As PuzzleData)
        Dim xCol As Integer, yRow As Integer
        Dim I As Integer
        Dim dpLeft As Integer, dpTop As Integer
        Dim strChar As String

        Dim bmpCanvas As New Bitmap(picField.Width, picField.Height)
        Dim g As Graphics = Graphics.FromImage(bmpCanvas)
        Dim fnt As New Font(Me.Font.Name, 12)

        With objPuzzle
            drawFieldLines(.m_numCols, .m_numRows, g)

            ' 数字を表示する。
            For I = 1 To .m_numWords
                With .m_aryWords(I)
                    xCol = .xCol - 1
                    yRow = .yRow - 1
                    dpLeft = (xCol * m_cxCellWidth) + m_fyTopMargin
                    dpTop = (yRow * m_cyCellHeight) + m_fyTopMargin
                    g.DrawString(I.ToString(), Me.Font, Brushes.Black, dpLeft, dpTop)
                End With
            Next

            ' フィールド上の文字を表示する。
            For yRow = 0 To .m_numRows - 1
                dpTop = (yRow * m_cyCellHeight) + m_fyTopMargin
                For xCol = 0 To .m_numCols - 1
                    dpLeft = (xCol * m_cxCellWidth) + m_fxLeftMargin

                    strChar = .m_aryField(xCol + 1, yRow + 1)
                    Dim chrSize As System.Drawing.SizeF = g.MeasureString(strChar, fnt)
                    g.DrawString(strChar, fnt, Brushes.Black, _
                                 dpLeft + (m_cxCellWidth - chrSize.Width) \ 2, _
                                 dpTop + (m_cyCellHeight - chrSize.Height))
                Next
            Next
        End With

        ' リストで選択した文字は強調表示する。
        If (m_nCurSelectIdx >= 0) Then
            highlightSelectedWord(m_nCurSelectIdx, objPuzzle, g)
        End If

        fnt.Dispose()
        g.Dispose()
        picField.Image = bmpCanvas
    End Sub

    ''-------------------------------------------------------------------------
    ''    単語一覧を更新する。
    ''-------------------------------------------------------------------------
    Private Sub updateWordsList(ByRef objPuzzle As PuzzleData)
        Dim I As Integer, numWord As Integer, nLen As Integer
        Dim strTemp As String

        numWord = objPuzzle.m_numWords
        PuzzleSolver.checkValidDirections(objPuzzle)

        With lstWords
            .Items.Clear()
            For I = 1 To numWord
                nLen = objPuzzle.m_aryWords(I).nLen
                If (objPuzzle.m_aryWrite(I).nLen = nLen) Then
                    strTemp = "レ"
                Else
                    strTemp = "□"
                End If

                strTemp = strTemp & String.Format("{0,3:#0}", I)
                Dim iDir As PuzzleData.Direction = objPuzzle.m_aryWords(I).iDir
                If (iDir <> PuzzleData.Direction.DIR_NONE) Then
                    strTemp = strTemp & Mid("↑←→↓", iDir, 1)
                End If
                strTemp = strTemp & " "
                For J = 0 To nLen - 1
                    strTemp = strTemp & objPuzzle.m_aryWords(I).sChrs(J)
                Next J
                .Items.Add(strTemp)
            Next I
        End With
    End Sub

    Private Sub lstWords_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstWords.SelectedIndexChanged
        m_nCurSelectIdx = lstWords.SelectedIndex
        updateField(m_objPuzzle)
    End Sub

    Private Sub mnuFileExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuFileExit.Click
        If MsgBox("終了します", MsgBoxStyle.OkCancel) = MsgBoxResult.Ok Then
            Me.Close()
            End
        End If
    End Sub

    Private Sub mnuFileOpen_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuFileOpen.Click
        With dlgOpenFile
            .InitialDirectory = ""
            .Filter = "テキストファイル(*.txt)|*.txt|すべてのファイル(*.*)|*.*"
            .FilterIndex = 1
        End With

        If dlgOpenFile.ShowDialog() = DialogResult.Cancel Then
            Exit Sub
        End If

        m_objPuzzle = New PuzzleData
        If (m_objPuzzle.readDataFromFile(dlgOpenFile.FileName) = False) Then
            Exit Sub
        End If

        ' メニューを有効にする。
        mnuFileSave.Enabled = True
        mnuSolve.Enabled = True

        ' 画面を更新する。
        resizeField(m_objPuzzle)
        PuzzleSolver.checkCompleteWords(m_objPuzzle)
        updateWordsList(m_objPuzzle)
        updateField(m_objPuzzle)
    End Sub

    Private Sub mnuFileSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuFileSave.Click
        With dlgSaveFile
            .InitialDirectory = ""
            .Filter = "テキストファイル(*.txt)|*.txt|すべてのファイル(*.*)|*.*"
            .FilterIndex = 1
        End With

        If dlgSaveFile.ShowDialog() = DialogResult.Cancel Then
            Exit Sub
        End If
        MsgBox("セーブ完了。")
    End Sub

    Private Sub mnuSolveDet_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSolveDet.Click
        Dim nCurSel As Integer = lstWords.SelectedIndex
        Dim retRun As PuzzleSolver.SolveWriteResult

        Do
            retRun = PuzzleSolver.writeDeterministicChars(m_objPuzzle)
            If (retRun = SolveWriteResult.RET_ERROR_COLLISION) Then
                MsgBox("矛盾が発生しました。")
                Exit Do
            End If

            PuzzleSolver.checkCompleteWords(m_objPuzzle)
            updateWordsList(m_objPuzzle)
            lstWords.SelectedIndex = nCurSel
            updateField(m_objPuzzle)

            System.Threading.Thread.Sleep(100)
            Application.DoEvents()
        Loop While (retRun = SolveWriteResult.RET_WRITE_SOME_CHARS)

        MsgBox("確定文字を書き込みました。")
    End Sub

    Private Sub mnuSolveTrial_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSolveTrial.Click
        ReDim Preserve m_objSaveStack(0 To m_nSaveSP)
        m_objSaveStack(m_nSaveSP) = New PuzzleData
        m_objSaveStack(m_nSaveSP).copyFrom(m_objPuzzle)
        m_nSaveSP = m_nSaveSP + 1
    End Sub

    Private Sub mnuSolveSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSolveSearch.Click
        Dim idxWord As Integer
        Dim dirNext As PuzzleData.Direction

        ' 背理法を試みる場所を探す。
        If Not (PuzzleSolver.searchTrialCandidate(m_objPuzzle, idxWord, dirNext)) Then
            MsgBox("候補地が見つかりませんでした。")
            Exit Sub
        End If

        MsgBox("単語 = " & idxWord & ", 方向 = " & dirNext)
        With m_objPuzzle.m_aryWrite(idxWord)
            With .pData(.nLen - 1)
                Select Case dirNext
                    Case PuzzleData.Direction.DIR_UP
                        .dirSet = .dirSet And Not (PuzzleData.DirSet.DIRSET_UP)
                    Case PuzzleData.Direction.DIR_LEFT
                        .dirSet = .dirSet And Not (PuzzleData.DirSet.DIRSET_LEFT)
                    Case PuzzleData.Direction.DIR_RIGHT
                        .dirSet = .dirSet And Not (PuzzleData.DirSet.DIRSET_RIGHT)
                    Case PuzzleData.Direction.DIR_DOWN
                        .dirSet = .dirSet And Not (PuzzleData.DirSet.DIRSET_DOWN)
                End Select
            End With
        End With

        updateField(m_objPuzzle)
    End Sub
End Class
