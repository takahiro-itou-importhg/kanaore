﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMainForm
    Inherits System.Windows.Forms.Form

    'フォームがコンポーネントの一覧をクリーンアップするために dispose をオーバーライドします。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows フォーム デザイナで必要です。
    Private components As System.ComponentModel.IContainer

    'メモ: 以下のプロシージャは Windows フォーム デザイナで必要です。
    'Windows フォーム デザイナを使用して変更できます。  
    'コード エディタを使って変更しないでください。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.mnuMainForm = New System.Windows.Forms.MenuStrip
        Me.mnuFile = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuFileOpen = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuFileSave = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuFileSep0 = New System.Windows.Forms.ToolStripSeparator
        Me.mnuFileExit = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuSolve = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuSolveDet = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuSolveTrial = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuSolveSearch = New System.Windows.Forms.ToolStripMenuItem
        Me.dlgOpenFile = New System.Windows.Forms.OpenFileDialog
        Me.dlgSaveFile = New System.Windows.Forms.SaveFileDialog
        Me.picField = New System.Windows.Forms.PictureBox
        Me.lstWords = New System.Windows.Forms.ListBox
        Me.mnuMainForm.SuspendLayout()
        CType(Me.picField, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'mnuMainForm
        '
        Me.mnuMainForm.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuFile, Me.mnuSolve})
        Me.mnuMainForm.Location = New System.Drawing.Point(0, 0)
        Me.mnuMainForm.Name = "mnuMainForm"
        Me.mnuMainForm.Size = New System.Drawing.Size(574, 24)
        Me.mnuMainForm.TabIndex = 0
        '
        'mnuFile
        '
        Me.mnuFile.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuFileOpen, Me.mnuFileSave, Me.mnuFileSep0, Me.mnuFileExit})
        Me.mnuFile.Name = "mnuFile"
        Me.mnuFile.Size = New System.Drawing.Size(66, 20)
        Me.mnuFile.Text = "ファイル(&F)"
        '
        'mnuFileOpen
        '
        Me.mnuFileOpen.Name = "mnuFileOpen"
        Me.mnuFileOpen.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.O), System.Windows.Forms.Keys)
        Me.mnuFileOpen.Size = New System.Drawing.Size(148, 22)
        Me.mnuFileOpen.Text = "開く(&O)"
        '
        'mnuFileSave
        '
        Me.mnuFileSave.Enabled = False
        Me.mnuFileSave.Name = "mnuFileSave"
        Me.mnuFileSave.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.S), System.Windows.Forms.Keys)
        Me.mnuFileSave.Size = New System.Drawing.Size(148, 22)
        Me.mnuFileSave.Text = "保存(&S)"
        '
        'mnuFileSep0
        '
        Me.mnuFileSep0.Name = "mnuFileSep0"
        Me.mnuFileSep0.Size = New System.Drawing.Size(145, 6)
        '
        'mnuFileExit
        '
        Me.mnuFileExit.Name = "mnuFileExit"
        Me.mnuFileExit.ShortcutKeys = CType((System.Windows.Forms.Keys.Alt Or System.Windows.Forms.Keys.F4), System.Windows.Forms.Keys)
        Me.mnuFileExit.Size = New System.Drawing.Size(148, 22)
        Me.mnuFileExit.Text = "終了(&X)"
        '
        'mnuSolve
        '
        Me.mnuSolve.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuSolveDet, Me.mnuSolveTrial, Me.mnuSolveSearch})
        Me.mnuSolve.Enabled = False
        Me.mnuSolve.Name = "mnuSolve"
        Me.mnuSolve.Size = New System.Drawing.Size(56, 20)
        Me.mnuSolve.Text = "解答(&S)"
        '
        'mnuSolveDet
        '
        Me.mnuSolveDet.Name = "mnuSolveDet"
        Me.mnuSolveDet.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.D), System.Windows.Forms.Keys)
        Me.mnuSolveDet.Size = New System.Drawing.Size(212, 22)
        Me.mnuSolveDet.Text = "確定文字を埋める(&D)"
        '
        'mnuSolveTrial
        '
        Me.mnuSolveTrial.Enabled = False
        Me.mnuSolveTrial.Name = "mnuSolveTrial"
        Me.mnuSolveTrial.Size = New System.Drawing.Size(212, 22)
        Me.mnuSolveTrial.Text = "背理法を使う(&T)"
        '
        'mnuSolveSearch
        '
        Me.mnuSolveSearch.Name = "mnuSolveSearch"
        Me.mnuSolveSearch.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.T), System.Windows.Forms.Keys)
        Me.mnuSolveSearch.Size = New System.Drawing.Size(235, 22)
        Me.mnuSolveSearch.Text = "背理法の候補地を探す(&S)"
        '
        'picField
        '
        Me.picField.BackColor = System.Drawing.Color.White
        Me.picField.Location = New System.Drawing.Point(221, 45)
        Me.picField.Name = "picField"
        Me.picField.Size = New System.Drawing.Size(341, 307)
        Me.picField.TabIndex = 1
        Me.picField.TabStop = False
        '
        'lstWords
        '
        Me.lstWords.Font = New System.Drawing.Font("ＭＳ ゴシック", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lstWords.FormattingEnabled = True
        Me.lstWords.Location = New System.Drawing.Point(12, 48)
        Me.lstWords.Name = "lstWords"
        Me.lstWords.Size = New System.Drawing.Size(203, 303)
        Me.lstWords.TabIndex = 2
        '
        'frmMainForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(574, 364)
        Me.Controls.Add(Me.lstWords)
        Me.Controls.Add(Me.picField)
        Me.Controls.Add(Me.mnuMainForm)
        Me.Name = "frmMainForm"
        Me.Text = "Kanaore"
        Me.mnuMainForm.ResumeLayout(False)
        Me.mnuMainForm.PerformLayout()
        CType(Me.picField, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents mnuMainForm As System.Windows.Forms.MenuStrip
    Friend WithEvents dlgOpenFile As System.Windows.Forms.OpenFileDialog
    Friend WithEvents dlgSaveFile As System.Windows.Forms.SaveFileDialog
    Friend WithEvents picField As System.Windows.Forms.PictureBox
    Friend WithEvents mnuFile As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileOpen As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileSave As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileSep0 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuFileExit As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents lstWords As System.Windows.Forms.ListBox
    Friend WithEvents mnuSolve As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSolveDet As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSolveTrial As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSolveSearch As System.Windows.Forms.ToolStripMenuItem

End Class
